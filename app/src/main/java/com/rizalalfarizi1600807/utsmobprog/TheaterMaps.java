package com.rizalalfarizi1600807.utsmobprog;

import android.Manifest;
import android.content.pm.PackageManager;
import android.location.Location;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.rizalalfarizi1600807.utsmobprog.adapter.InfoWindowAdapter;
import com.rizalalfarizi1600807.utsmobprog.model.Movie;
import com.rizalalfarizi1600807.utsmobprog.model.Theater;

public class TheaterMaps extends FragmentActivity implements OnMapReadyCallback,GoogleApiClient.ConnectionCallbacks,GoogleApiClient.OnConnectionFailedListener {
    private static final int MY_PERMISSION_REQUEST = 1;

    private GoogleMap mMap;
    private GoogleApiClient googleApiClient;
    private Location lastLocation;
    private LatLng myCoord;

    public static final String EXTRA_MAP = "extra_map";
    public static final String EXTRA_MAP_MOVIE = "extra_map_movies";
    private Theater theater;
    private Movie movie;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_theater_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        movie = getIntent().getParcelableExtra(EXTRA_MAP_MOVIE);
        theater = getIntent().getParcelableExtra(EXTRA_MAP);

    }

    public synchronized void buildGoogleApiClient(){
        googleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        googleApiClient.connect();
    }

    protected void getLocation(){
        if(ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)!= PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.ACCESS_COARSE_LOCATION},MY_PERMISSION_REQUEST);
            return;
        }

        lastLocation = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);
        if(lastLocation != null){


            LatLng loc = new LatLng(theater.getLat(), theater.getLn());
            myCoord = new LatLng(lastLocation.getLatitude(),lastLocation.getLongitude());

//            InfoWindowAdapter customInfo = new InfoWindowAdapter(this,movie);
//            mMap.setInfoWindowAdapter(customInfo);

//            Marker marker =  mMap.addMarker(new MarkerOptions().position(loc).title(theater.getName()));
//            marker.showInfoWindow();
//
//            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(loc,14));

//            mMap.addMarker(new MarkerOptions().position(myCoord).title("Your Location"));
//            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(myCoord,17));
        }
    }



    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        LatLng loc = new LatLng(theater.getLat(), theater.getLn());

        InfoWindowAdapter customInfo = new InfoWindowAdapter(this);
        mMap.setInfoWindowAdapter(customInfo);

        Marker marker =  mMap.addMarker(new MarkerOptions().position(loc).title(theater.getName()));
        marker.setTag(movie);


        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(loc,14));


        //buildGoogleApiClient();

    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        getLocation();
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }
    @Override
    protected void onStop() {
        super.onStop();
//        googleApiClient.disconnect();
    }

    @Override
    protected void onStart() {
        super.onStart();

    }

}
