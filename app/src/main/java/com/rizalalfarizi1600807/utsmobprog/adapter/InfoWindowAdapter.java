package com.rizalalfarizi1600807.utsmobprog.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Marker;
import com.rizalalfarizi1600807.utsmobprog.R;
import com.rizalalfarizi1600807.utsmobprog.model.Movie;
import com.rizalalfarizi1600807.utsmobprog.network.ApiClient;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class InfoWindowAdapter implements GoogleMap.InfoWindowAdapter {

    private Context context;
    private TextView tvTitle,tvYear;
    private ImageView ivPoster;

    public InfoWindowAdapter(Context ctx){
        context = ctx;
    }


    @Override
    public View getInfoWindow(Marker marker) {
        return null;
    }

    @Override
    public View getInfoContents(Marker marker) {
        View view = ((Activity)context).getLayoutInflater().inflate(R.layout.custom_info_window,null);

        tvTitle = view.findViewById(R.id.title);
        tvYear = view.findViewById(R.id.year);
        ivPoster = view.findViewById(R.id.poster);
        Movie movie = (Movie) marker.getTag();

        tvTitle.setText(movie.getTitle());
        Glide.with(view)
                .load(ApiClient.IMAGE_BASE_URL + movie.getThumbPath())
                .into(ivPoster);
        SimpleDateFormat df = new SimpleDateFormat("yyyy");
        SimpleDateFormat format = new SimpleDateFormat("yyyy-mm-dd");

        try {
            Date date = format.parse(movie.getReleaseDate());
            tvYear.setText("("+df.format(date)+")");
        } catch (ParseException e) {
            e.printStackTrace();
        }



        return view;
    }


}
