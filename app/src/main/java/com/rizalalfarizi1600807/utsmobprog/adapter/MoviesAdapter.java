package com.rizalalfarizi1600807.utsmobprog.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.rizalalfarizi1600807.utsmobprog.R;
import com.rizalalfarizi1600807.utsmobprog.helper.utils;
import com.rizalalfarizi1600807.utsmobprog.model.Movie;
import com.rizalalfarizi1600807.utsmobprog.movie_list.MainActivity;
import com.rizalalfarizi1600807.utsmobprog.network.ApiClient;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class MoviesAdapter extends RecyclerView.Adapter<MoviesAdapter.MoviesViewHolder> {

    private MainActivity movieListActivity;
    private List<Movie> movieList;

    public MoviesAdapter(MainActivity movieListActivity, List<Movie> movieList) {
        this.movieListActivity = movieListActivity;
        this.movieList = movieList;
    }

    @NonNull
    @Override
    public MoviesViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_movie,parent,false);
        return new MoviesViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MoviesViewHolder holder, final int position) {
        Movie movie = movieList.get(position);

        String genre = utils.getGenre(movie.getGenre());
        holder.tvGenre.setText(genre);
        holder.tvTitle.setText(movie.getTitle());


        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        DateFormat df = new SimpleDateFormat("d MMMM yyyy");
        try {
            Date date = format.parse(movie.getReleaseDate());
            holder.tvReleaseDate.setText(df.format(date));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        Glide.with(movieListActivity)
                .load(ApiClient.IMAGE_BASE_URL + movie.getThumbPath())
                .into(holder.ivPoster);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                movieListActivity.onMovieItemClick(position);
            }
        });


    }

    @Override
    public int getItemCount() {
        return movieList.size();
    }

    public class MoviesViewHolder extends RecyclerView.ViewHolder{
        TextView tvTitle;
        TextView tvReleaseDate;
        TextView tvGenre;
        ImageView ivPoster;


        public MoviesViewHolder(View itemView) {
            super(itemView);
            tvTitle = itemView.findViewById(R.id.title);
            tvReleaseDate = itemView.findViewById(R.id.release_date);
            tvGenre = itemView.findViewById(R.id.genre);
            ivPoster = itemView.findViewById(R.id.poster);

        }
    }
}
