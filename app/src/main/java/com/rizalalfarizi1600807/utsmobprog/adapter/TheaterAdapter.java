package com.rizalalfarizi1600807.utsmobprog.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.rizalalfarizi1600807.utsmobprog.R;
import com.rizalalfarizi1600807.utsmobprog.model.Theater;
import com.rizalalfarizi1600807.utsmobprog.teather_list.TheaterActivity;

import java.util.List;

public class TheaterAdapter extends RecyclerView.Adapter<TheaterAdapter.TheaterViewHolder> {

    private TheaterActivity theaterActivity;
    private List<Theater> theaterList;

    public TheaterAdapter(TheaterActivity theaterActivity, List<Theater> theaterList) {
        this.theaterActivity = theaterActivity;
        this.theaterList = theaterList;
    }


    @NonNull
    @Override
    public TheaterViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_row_theater,parent,false);
        return new TheaterViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull TheaterViewHolder holder, final int position) {
        Theater theater = theaterList.get(position);

        holder.tvTheater.setText(theater.getName());
        if(theater.getCategory()!="xxi"){
            holder.ivLogo.setImageDrawable(theaterActivity.getResources().getDrawable(R.drawable.cgv_logo));
        }else{
            holder.ivLogo.setImageDrawable(theaterActivity.getResources().getDrawable(R.drawable.cinema21_logo));
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                theaterActivity.onTheaterItemClick(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return theaterList.size();
    }

    public class TheaterViewHolder extends RecyclerView.ViewHolder{
        ImageView ivLogo;
        TextView tvTheater;

        public TheaterViewHolder(View itemView) {
            super(itemView);
            ivLogo = itemView.findViewById(R.id.logo);
            tvTheater = itemView.findViewById(R.id.theater_name);
        }
    }
}
