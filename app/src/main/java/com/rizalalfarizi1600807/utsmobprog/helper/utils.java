package com.rizalalfarizi1600807.utsmobprog.helper;

import com.rizalalfarizi1600807.utsmobprog.model.Genre;

import java.util.ArrayList;
import java.util.List;

public class utils {
    public static ArrayList<Genre> getAllGenre(){
        ArrayList<Genre> genres = new ArrayList<Genre>();
        Genre g = new Genre(28,"Action");
        genres.add(g);
        g = new Genre(28,"Action");
        genres.add(g);
        g = new Genre(12,"Adventure");
        genres.add(g);
        g = new Genre(16,"Animation");
        genres.add(g);
        g = new Genre(35,"Comedy");
        genres.add(g);
        g = new Genre(80,"Crime");
        genres.add(g);
        g = new Genre(99,"Documentary");
        genres.add(g);
        g = new Genre(18,"Drama");
        genres.add(g);
        g = new Genre(10715,"Family");
        genres.add(g);
        g = new Genre(14,"Fantasy");
        genres.add(g);
        g = new Genre(36,"History");
        genres.add(g);
        g = new Genre(27,"Horror");
        genres.add(g);
        g = new Genre(10402,"Music");
        genres.add(g);
        g = new Genre(9648,"Mystery");
        genres.add(g);
        g = new Genre(10749,"Romance");
        genres.add(g);
        g = new Genre(878,"Science Fiction");
        genres.add(g);
        g = new Genre(10770,"TV Movie");
        genres.add(g);
        g = new Genre(53,"Thriller");
        genres.add(g);
        g = new Genre(10752,"War");
        genres.add(g);
        g = new Genre(37,"Western");
        genres.add(g);
        return genres;
    }
    public static String getGenre(int[] id){

        String res = "";

        if (id != null ) {
            int i = 0;
            ArrayList<Genre> gen = getAllGenre();

            for (int j = 0; j < id.length; j++){
                for (Genre g:gen) {
                    if(g.getId() == id[j]){
                        if(i==0){
                            res += g.getName();
                        }else{
                            res += ", " + g.getName();
                        }
                        i++;
                    }
                }
            }
        }
        return res;
    }

    public static String getGenre(List<Genre> id){

        String res = "";

        if (id != null ) {
            int i = 0;
            ArrayList<Genre> gen = getAllGenre();
            for (Genre j: id){
                for (Genre g:gen) {
                    if(g.getId() == j.getId()){
                        if(i==0){
                            res += g.getName();
                        }else{
                            res += ", " + g.getName();
                        }
                        i++;
                    }
                }
            }
        }
        return res;
    }
}
