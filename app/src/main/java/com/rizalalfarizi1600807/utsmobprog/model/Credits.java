package com.rizalalfarizi1600807.utsmobprog.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Credits {
    @SerializedName("crew")
    List<Crew> crews;

    @SerializedName("cast")
    List<Cast> casts;

    public List<Crew> getCrews() {
        return crews;
    }

    public void setCrews(List<Crew> crews) {
        this.crews = crews;
    }

    public List<Cast> getCasts() {
        return casts;
    }

    public void setCasts(List<Cast> casts) {
        this.casts = casts;
    }
}
