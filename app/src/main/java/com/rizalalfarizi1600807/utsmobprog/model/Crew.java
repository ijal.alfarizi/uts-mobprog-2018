package com.rizalalfarizi1600807.utsmobprog.model;

import com.google.gson.annotations.SerializedName;

public class Crew {
    @SerializedName("name")
    private String name;

    @SerializedName("department")
    private String departmen;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDepartmen() {
        return departmen;
    }

    public void setDepartmen(String departmen) {
        this.departmen = departmen;
    }
}
