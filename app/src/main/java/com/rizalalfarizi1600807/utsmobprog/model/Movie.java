package com.rizalalfarizi1600807.utsmobprog.model;


import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class Movie implements Parcelable {

    private int id;

    private String title;

    @SerializedName("genre_ids")
    private int[] genre;

    @SerializedName("genres")
    private List<Genre> genre_id;

    @SerializedName("original_language")
    private String original_language;

    @SerializedName("runtime")
    private int runtime;

    @SerializedName("credits")
    private Credits credits;


    @SerializedName("release_date")
    private String releaseDate;

    @SerializedName("vote_average")
    private float rating;

    @SerializedName("poster_path")
    private String thumbPath;

    @SerializedName("overview")
    private String overview;

    @SerializedName("backdrop_path")
    private String backdropPath;

//    @SerializedName("credits")
//    private Credits credits;


    @SerializedName("tagline")
    private String tagline;

    @SerializedName("homepage")
    private String homepage;

    public Movie(int id, String title, String releaseDate, float rating, String thumbPath, String overview, String backdropPath, String runTime, String tagline, String homepage) {
        this.id = id;
        this.title = title;
        this.releaseDate = releaseDate;
        this.rating = rating;
        this.thumbPath = thumbPath;
        this.overview = overview;
        this.backdropPath = backdropPath;
//        this.credits = credits;
        this.tagline = tagline;
        this.homepage = homepage;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(String releaseDate) {
        this.releaseDate = releaseDate;
    }

    public float getRating() {
        return rating;
    }

    public void setRating(float rating) {
        this.rating = rating;
    }

    public String getThumbPath() {
        return thumbPath;
    }

    public void setThumbPath(String thumbPath) {
        this.thumbPath = thumbPath;
    }

    public String getOverview() {
        return overview;
    }

    public void setOverview(String overview) {
        this.overview = overview;
    }

    public String getBackdropPath() {
        return backdropPath;
    }

    public void setBackdropPath(String backdropPath) {
        this.backdropPath = backdropPath;
    }

//    public Credits getCredits() {
//        return credits;
//    }
//
//    public void setCredits(Credits credits) {
//        this.credits = credits;
//    }



    public String getTagline() {
        return tagline;
    }

    public void setTagline(String tagline) {
        this.tagline = tagline;
    }

    public String getHomepage() {
        return homepage;
    }

    public void setHomepage(String homepage) {
        this.homepage = homepage;
    }

    @Override
    public String toString() {
        return "Movie{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", releaseDate='" + releaseDate + '\'' +
                ", rating=" + rating +
                ", thumbPath='" + thumbPath + '\'' +
                ", overview='" + overview + '\'' +
                ", backdropPath='" + backdropPath + '\'' +
//                ", credits=" + credits +
                ", tagline='" + tagline + '\'' +
                ", homepage='" + homepage + '\'' +
                '}';
    }

    public int[] getGenre() {
        return genre;
    }

    public void setGenre(int[] genre) {
        this.genre = genre;
    }

    public List<Genre> getGenre_id() {
        return genre_id;
    }

    public void setGenre_id(List<Genre> genre_id) {
        this.genre_id = genre_id;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeString(this.title);
        dest.writeIntArray(this.genre);
//        dest.writeList(this.genre_id);
        dest.writeString(this.releaseDate);
        dest.writeFloat(this.rating);
        dest.writeString(this.thumbPath);
        dest.writeString(this.overview);
        dest.writeString(this.backdropPath);
        dest.writeString(this.tagline);
        dest.writeString(this.homepage);
    }

    protected Movie(Parcel in) {
        this.id = in.readInt();
        this.title = in.readString();
        this.genre = in.createIntArray();
        this.genre_id = new ArrayList<Genre>();
//        in.readList(this.genre_id, Genre.class.getClassLoader());
        this.releaseDate = in.readString();
        this.rating = in.readFloat();
        this.thumbPath = in.readString();
        this.overview = in.readString();
        this.backdropPath = in.readString();
        this.tagline = in.readString();
        this.homepage = in.readString();
    }

    public static final Parcelable.Creator<Movie> CREATOR = new Parcelable.Creator<Movie>() {
        @Override
        public Movie createFromParcel(Parcel source) {
            return new Movie(source);
        }

        @Override
        public Movie[] newArray(int size) {
            return new Movie[size];
        }
    };

    public String getOriginal_language() {
        return original_language;
    }

    public void setOriginal_language(String original_language) {
        this.original_language = original_language;
    }

    public int getRuntime() {
        return runtime;
    }

    public void setRuntime(int runtime) {
        this.runtime = runtime;
    }

    public Credits getCredits() {
        return credits;
    }

    public void setCredits(Credits credits) {
        this.credits = credits;
    }
}
