package com.rizalalfarizi1600807.utsmobprog.model;

import android.os.Parcel;
import android.os.Parcelable;

public class Theater implements Parcelable {
    private String name;
    private double lat;
    private double ln;
    private String category;
    private String poster;
    private Movie movie;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLn() {
        return ln;
    }

    public void setLn(double ln) {
        this.ln = ln;
    }

    public String getPoster() {
        return poster;
    }

    public void setPoster(String movie) {
        this.poster = movie;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.name);
        dest.writeDouble(this.lat);
        dest.writeDouble(this.ln);
        dest.writeString(this.category);
        dest.writeString(this.poster);
    }

    public Theater() {
    }

    protected Theater(Parcel in) {
        this.name = in.readString();
        this.lat = in.readDouble();
        this.ln = in.readDouble();
        this.category = in.readString();
        this.poster = in.readString();
    }

    public static final Parcelable.Creator<Theater> CREATOR = new Parcelable.Creator<Theater>() {
        @Override
        public Theater createFromParcel(Parcel source) {
            return new Theater(source);
        }

        @Override
        public Theater[] newArray(int size) {
            return new Theater[size];
        }
    };

    public Movie getMovie() {
        return movie;
    }

    public void setMovie(Movie movie) {
        this.movie = movie;
    }
}
