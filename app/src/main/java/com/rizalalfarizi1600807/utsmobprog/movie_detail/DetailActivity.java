package com.rizalalfarizi1600807.utsmobprog.movie_detail;

import android.content.Intent;
import android.media.Image;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.rizalalfarizi1600807.utsmobprog.R;
import com.rizalalfarizi1600807.utsmobprog.helper.utils;
import com.rizalalfarizi1600807.utsmobprog.model.Genre;
import com.rizalalfarizi1600807.utsmobprog.model.Movie;
import com.rizalalfarizi1600807.utsmobprog.network.ApiClient;
import com.rizalalfarizi1600807.utsmobprog.teather_list.TheaterActivity;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DetailActivity extends AppCompatActivity implements MovieDetailContract.View,View.OnClickListener {

    public static String EXTRAS_MOVIE = "extra_movie";

    private ImageView ivPoster,ivBackdrop;
    private TextView tvTitle,tvYear,tvGenre,tvRating,tvOverview,tvReleaseDate,tvRuntime,tvLanguage,tvCast,tvCrew;
    private Button btnTheater;

    private Movie movie;
    private MovieDetailPresenter movieDetailPresenter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        initUI();
        int movieId = getIntent().getIntExtra(EXTRAS_MOVIE, 0);

        movieDetailPresenter = new MovieDetailPresenter(this);
        movieDetailPresenter.requestMovieData(movieId);

        btnTheater.setOnClickListener(this);

    }

    public void initUI(){
        ivBackdrop = findViewById(R.id.backdrop);
        ivPoster = findViewById(R.id.poster);
        tvGenre = findViewById(R.id.genre);
        tvOverview = findViewById(R.id.overview);
        tvTitle = findViewById(R.id.title);
        tvYear = findViewById(R.id.year);
        tvRating = findViewById(R.id.rating);
        tvReleaseDate = findViewById(R.id.release_date);
        btnTheater = findViewById(R.id.btn_theater);
        tvLanguage = findViewById(R.id.language);
        tvRuntime = findViewById(R.id.runtime);
        tvCast= findViewById(R.id.cast);
        tvCrew= findViewById(R.id.crew);

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        movieDetailPresenter.onDestroy();

    }

    @Override
    public void setDataToViews(Movie movie) {
        if(movie!=null){
            this.movie = movie;
            String title = movie.getTitle();
            if(title.length() > 20)
                title = title.substring(0,20) + ". . .";
            tvTitle.setText(title);
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            DateFormat df = new SimpleDateFormat("d MMMM yyyy");
            try {
                Date date = format.parse(movie.getReleaseDate());
                tvReleaseDate.setText(df.format(date));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            tvRating.setText(String.valueOf(movie.getRating()));
            df = new SimpleDateFormat("yyyy");
            try {
                Date date = format.parse(movie.getReleaseDate());
                tvYear.setText(df.format(date));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            String cast="";
            for (int i = 0; i<4; i++){
                cast += movie.getCredits().getCasts().get(i).getName()+", ";
            }

            cast += " and " + (movie.getCredits().getCasts().size()-4) +" more.";

            String crew="";
            for (int i = 0; i<5; i++){
                crew += movie.getCredits().getCrews().get(i).getName()+"("+movie.getCredits().getCrews().get(i).getDepartmen()+"), ";
            }

            crew += " and " + (movie.getCredits().getCasts().size()-5) +" more.";

            tvCrew.setText(crew);
            tvCast.setText(cast);

            tvRuntime.setText(movie.getRuntime()+"min");
            tvLanguage.setText(movie.getOriginal_language());

            String genre = utils.getGenre(movie.getGenre_id());
            tvGenre.setText(genre);
            tvOverview.setText(movie.getOverview());
            Glide.with(this)
                    .load(ApiClient.IMAGE_BASE_URL + movie.getThumbPath())
                    .into(ivPoster);
            Glide.with(this)
                    .load(ApiClient.BACKDROP_BASE_URL + movie.getBackdropPath())
                    .into(ivBackdrop);
        }
    }

    @Override
    public void onResponseFailure(Throwable throwable) {
        Toast.makeText(this,"Terjadi kesalahan...", Toast.LENGTH_LONG);
    }

    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.btn_theater){
            Intent intent = new Intent(this, TheaterActivity.class);
            intent.putExtra(TheaterActivity.EXTRAS_MOVIE, movie);
            startActivity(intent);
        }
    }
}
