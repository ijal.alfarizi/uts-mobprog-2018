package com.rizalalfarizi1600807.utsmobprog.movie_detail;

import com.rizalalfarizi1600807.utsmobprog.model.Movie;
import com.rizalalfarizi1600807.utsmobprog.network.ApiClient;
import com.rizalalfarizi1600807.utsmobprog.network.ApiInterface;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.rizalalfarizi1600807.utsmobprog.network.ApiClient.API_KEY;

public class MovieDetailModel implements MovieDetailContract.Model {
    @Override
    public void getMovieDetails(final OnFinishedListener onFinishedListener, int movieId) {
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);

        Call<Movie> call = apiService.getMovieDetails(movieId, API_KEY, "credits");
        call.enqueue(new Callback<Movie>() {
            @Override
            public void onResponse(Call<Movie> call, Response<Movie> response) {
                Movie movie = response.body();
                onFinishedListener.onFinished(movie);
            }

            @Override
            public void onFailure(Call<Movie> call, Throwable t) {
                onFinishedListener.onFailure(t);
            }
        });
    }
}
