package com.rizalalfarizi1600807.utsmobprog.movie_detail;

import com.rizalalfarizi1600807.utsmobprog.model.Movie;

public class MovieDetailPresenter  implements MovieDetailContract.Presenter , MovieDetailContract.Model.OnFinishedListener {

    private MovieDetailContract.View movieDetailView;
    private MovieDetailContract.Model movieDetailsModel;

    public MovieDetailPresenter(MovieDetailContract.View movieDetailView) {
        this.movieDetailView = movieDetailView;
        this.movieDetailsModel = new MovieDetailModel();
    }

    @Override
    public void onFinished(Movie movie) {
        movieDetailView.setDataToViews(movie);
    }

    @Override
    public void onFailure(Throwable t) {
        movieDetailView.onResponseFailure(t);
    }

    @Override
    public void onDestroy() {
        movieDetailView = null;
    }

    @Override
    public void requestMovieData(int movieId) {
        movieDetailsModel.getMovieDetails(this,movieId);
    }
}
