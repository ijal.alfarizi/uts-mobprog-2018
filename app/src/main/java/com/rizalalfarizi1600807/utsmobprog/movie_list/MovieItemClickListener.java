package com.rizalalfarizi1600807.utsmobprog.movie_list;

public interface MovieItemClickListener {

    void onMovieItemClick(int position);
}
