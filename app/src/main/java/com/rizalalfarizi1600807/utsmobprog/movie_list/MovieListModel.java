package com.rizalalfarizi1600807.utsmobprog.movie_list;

import com.rizalalfarizi1600807.utsmobprog.model.Movie;
import com.rizalalfarizi1600807.utsmobprog.model.MovieListResponse;
import com.rizalalfarizi1600807.utsmobprog.network.ApiClient;
import com.rizalalfarizi1600807.utsmobprog.network.ApiInterface;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.rizalalfarizi1600807.utsmobprog.network.ApiClient.API_KEY;

public class MovieListModel implements MovieListContract.Model {

    @Override
    public void getMovieList(final OnFinishedListener onFinishedListener, int pageNo) {
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

        Call<MovieListResponse> call = apiService.getPopularMovies(API_KEY, pageNo);
        call.enqueue(new Callback<MovieListResponse>() {
            @Override
            public void onResponse(Call<MovieListResponse> call, Response<MovieListResponse> response) {
                List<Movie> movies = response.body().getResults();
                onFinishedListener.onFinished(movies);
            }

            @Override
            public void onFailure(Call<MovieListResponse> call, Throwable t) {
                onFinishedListener.onFailure(t);
            }
        });
    }
}
