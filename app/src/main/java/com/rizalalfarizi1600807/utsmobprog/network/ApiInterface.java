package com.rizalalfarizi1600807.utsmobprog.network;

import com.rizalalfarizi1600807.utsmobprog.model.Movie;
import com.rizalalfarizi1600807.utsmobprog.model.MovieListResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ApiInterface {

    //list of popular movie
    @GET("movie/popular")
    Call<MovieListResponse> getPopularMovies(@Query("api_key") String apiKey, @Query("page") int pageNo);

    //detail movie
    @GET("movie/{id}")
    Call<Movie> getMovieDetails(@Path("id") int movieId, @Query("api_key") String apiKey, @Query("append_to_response") String credits);
}
