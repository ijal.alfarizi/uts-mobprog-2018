package com.rizalalfarizi1600807.utsmobprog.teather_list;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.rizalalfarizi1600807.utsmobprog.R;
import com.rizalalfarizi1600807.utsmobprog.TheaterMaps;
import com.rizalalfarizi1600807.utsmobprog.adapter.TheaterAdapter;
import com.rizalalfarizi1600807.utsmobprog.model.Movie;
import com.rizalalfarizi1600807.utsmobprog.model.Theater;

import java.util.ArrayList;
import java.util.List;

public class TheaterActivity extends AppCompatActivity implements TheaterListContract.View,TheaterItemClickListener{
    public static final String EXTRAS_MOVIE = "extra_movie";
    private RecyclerView rvTheater;
    private LinearLayoutManager linearLayoutManager;
    private ArrayList<Theater> theaterList;
    private TheaterAdapter theaterAdapter;
    private TheaterListPresenter  theaterListPresenter;
    private Movie movie;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_theater);
        iniUI();

        movie = getIntent().getParcelableExtra(EXTRAS_MOVIE);
        theaterListPresenter = new TheaterListPresenter(this);
        theaterListPresenter.requestTheaterData();

    }

    public void iniUI(){
        rvTheater = findViewById(R.id.rv_theater);

        theaterList = new ArrayList<>();
        theaterAdapter = new TheaterAdapter(this, theaterList);

        linearLayoutManager = new LinearLayoutManager(this);
        rvTheater.setLayoutManager(linearLayoutManager);
        rvTheater.setAdapter(theaterAdapter);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        theaterListPresenter.onDestroy();
    }

    @Override
    public void setDataToViews(List<Theater> theater) {
        theaterList.addAll(theater);
        theaterAdapter.notifyDataSetChanged();
    }

    @Override
    public void onResponseFailure(Throwable throwable) {

    }

    @Override
    public void onTheaterItemClick(int position) {
        Theater t = theaterList.get(position);

        Intent intent = new Intent(this, TheaterMaps.class);
        intent.putExtra(TheaterMaps.EXTRA_MAP,t);
        intent.putExtra(TheaterMaps.EXTRA_MAP_MOVIE,movie);

        startActivity(intent);

    }
}
