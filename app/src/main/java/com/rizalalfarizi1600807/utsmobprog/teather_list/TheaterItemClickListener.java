package com.rizalalfarizi1600807.utsmobprog.teather_list;

public interface TheaterItemClickListener {
    void onTheaterItemClick(int position);
}
