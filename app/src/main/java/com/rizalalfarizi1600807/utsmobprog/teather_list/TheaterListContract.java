package com.rizalalfarizi1600807.utsmobprog.teather_list;

import com.rizalalfarizi1600807.utsmobprog.model.Theater;

import java.util.List;

public interface TheaterListContract {

    interface Model {

        interface OnFinishedListener {
            void onFinished(List<Theater> theaterArraylist);

            void onFailure(Throwable t);
        }

        void getTheaterList(TheaterListContract.Model.OnFinishedListener onFinishedListener);

    }
    interface View {
        void setDataToViews(List<Theater> theater );

        void onResponseFailure(Throwable throwable);
    }

    interface Presenter {
        void onDestroy();

        void requestTheaterData();
    }

}
