package com.rizalalfarizi1600807.utsmobprog.teather_list;

import com.rizalalfarizi1600807.utsmobprog.model.Theater;

import java.util.ArrayList;
import java.util.List;

public class TheaterListModel implements TheaterListContract.Model {


    @Override
    public void getTheaterList(OnFinishedListener onFinishedListener) {
        ArrayList<Theater> theaters = new ArrayList<>();
        Theater t = new Theater();
        t.setName("BTC XX1");
        t.setCategory("xxi");
        t.setLat(-6.8930026);
        t.setLn(107.5827022);
        theaters.add(t);

        t = new Theater();
        t.setName("CIWALK XX1");
        t.setCategory("xxi");
        t.setLat(-6.893196);
        t.setLn(107.6023293);
        theaters.add(t);

        t = new Theater();
        t.setName("BRAGA XX1");
        t.setCategory("xxi");
        t.setLat(-6.9171441);
        t.setLn(107.6066858);
        theaters.add(t);

        t = new Theater();
        t.setName("CGV BEC");
        t.setCategory("cgv");
        t.setLat(-6.9082098);
        t.setLn(107.6067256);
        theaters.add(t);

        t = new Theater();
        t.setName("CGV PVJ");
        t.setCategory("cgv");
        t.setLat(-6.8881615);
        t.setLn(107.5934793);
        theaters.add(t);


        t = new Theater();
        t.setName("CGV 23 PASKAL");
        t.setCategory("cgv");
        t.setLat(-6.9154162);
        t.setLn(107.5920725);
        theaters.add(t);

        onFinishedListener.onFinished(theaters);

    }
}
