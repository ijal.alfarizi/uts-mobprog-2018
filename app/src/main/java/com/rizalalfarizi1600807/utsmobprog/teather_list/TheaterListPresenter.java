package com.rizalalfarizi1600807.utsmobprog.teather_list;

import com.rizalalfarizi1600807.utsmobprog.model.Movie;
import com.rizalalfarizi1600807.utsmobprog.model.Theater;
import com.rizalalfarizi1600807.utsmobprog.movie_detail.MovieDetailContract;

import java.util.List;

public class TheaterListPresenter implements TheaterListContract.Presenter, TheaterListContract.Model.OnFinishedListener {

    private TheaterListContract.Model theaterListModel;
    private TheaterListContract.View theaterListView;

    public TheaterListPresenter(TheaterListContract.View theaterListView) {
        this.theaterListModel = new TheaterListModel();
        this.theaterListView = theaterListView;
    }


    @Override
    public void onFinished(List<Theater> theaterArraylist) {
        this.theaterListView.setDataToViews(theaterArraylist);
    }

    @Override
    public void onFailure(Throwable t) {
        theaterListView.onResponseFailure(t);
    }

    @Override
    public void onDestroy() {
        theaterListView = null;
    }

    @Override
    public void requestTheaterData() {
        theaterListModel.getTheaterList(this);
    }
}
